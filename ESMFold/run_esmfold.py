#!/bin/bash

while [[ $# -gt 0 ]]; do
  case $1 in
    -f|--fasta_dir)
      FASTADIR="$2"
      shift # past argument
      shift # past value
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done


source /cluster/apps/local/env2lmod.sh
module load gcc/8.2.0 python_gpu/3.9.9 

/cluster/home/lucstalder/scratch/ESM_Fold/ESMFold_euler.py --fasta_dir ${FASTADIR}