#!/usr/bin/env python
import os
import argparse
from transformers import AutoTokenizer, EsmForProteinFolding
import torch
from transformers.models.esm.openfold_utils.protein import to_pdb, Protein as OFProtein
from transformers.models.esm.openfold_utils.feats import atom14_to_atom37

#Function for converting outputs to pdb
def convert_outputs_to_pdb(outputs):
    final_atom_positions = atom14_to_atom37(outputs["positions"][-1], outputs)
    outputs = {k: v.to("cpu").numpy() for k, v in outputs.items()}
    final_atom_positions = final_atom_positions.cpu().numpy()
    final_atom_mask = outputs["atom37_atom_exists"]
    pdbs = []
    for i in range(outputs["aatype"].shape[0]):
        aa = outputs["aatype"][i]
        pred_pos = final_atom_positions[i]
        mask = final_atom_mask[i]
        resid = outputs["residue_index"][i] + 1
        pred = OFProtein(
            aatype=aa,
            atom_positions=pred_pos,
            atom_mask=mask,
            residue_index=resid,
            b_factors=outputs["plddt"][i],
            chain_index=outputs["chain_index"][i] if "chain_index" in outputs else None,
        )
        pdbs.append(to_pdb(pred))
    return pdbs


def import_fasta_sequences(path_to_dir):
    # Find all fasta files in a given directory
    fasta_files = [file for file in os.listdir(path_to_dir) if file.endswith('fasta') or file.endswith('fa')]

    # Read in all sequences in all the fasta files and store in a list
    name_lst = []
    seq_lst = []
    for file in fasta_files:
        with open(os.path.join(path_to_dir, file)) as f:
            txt = f.read()
            for seq in txt.split(">")[1:]:
                tmp_seq = seq.split("\n")
                name_lst.append(tmp_seq[0])
                seq_lst.append("".join(tmp_seq[1:]))

    
    return [name_lst, seq_lst]

def multimer_prediction(sequence):

    linker = 'G' * 25

    multimer_sequence = sequence.replace(":", linker)

    return multimer_sequence

p = argparse.ArgumentParser(description='Predict protein structures with ESMFold')
p.add_argument('--fasta_dir',
                    required=True,
                    help='Specify the path to a directory containing the FASTA files for prediction. ')
args = p.parse_args()




#Select tokenizer and model
tokenizer = AutoTokenizer.from_pretrained("/cluster/home/lucstalder/scratch/ESM_Fold/model")
model = EsmForProteinFolding.from_pretrained("/cluster/home/lucstalder/scratch/ESM_Fold/model", low_cpu_mem_usage=True)

#The model is transferred to GPU by model.cuda
model = model.cuda()

#Speed up computation
torch.backends.cuda.matmul.allow_tf32 = True


#Import Sequences to predict from fasta files

#Read in sequnces from fasta files in a directory
#Make sure it can read in multiple sequences in one fasta file


sequences_to_predict = import_fasta_sequences(args.fasta_dir)

for i in range(len(sequences_to_predict[1])):
    #Protein sequence is defined
    protein_seq = sequences_to_predict[1][i]

    #If protein is a multimer the a 25aa long glycine linker is introduced
    if ":" in protein_seq:
        #Get indices of the linker site
        idx_subunits = [i for i, aa in enumerate(protein_seq) if aa == ":"]
        #Exchange the ":" with 25G linker
        protein_seq = multimer_prediction(protein_seq)
        #Tokenize the input
        tokenized_multimer = tokenizer([protein_seq], return_tensors="pt", add_special_tokens=False)

        with torch.no_grad():
            position_ids = torch.arange(len(protein_seq), dtype=torch.long)
            mask_lst = len(protein_seq) * [1]
            for ii, idx in enumerate(idx_subunits):
                position_ids[idx + (ii+1)*25 -ii :] += 512
                #Get mask for removing residues later in the pdb
                mask_lst[(idx + ii * 25 - ii): (idx + ii * 25 - ii) + 25] = 25 * [0]


        #Add the new position ids
        tokenized_multimer['position_ids'] = position_ids.unsqueeze(0)

        #Transfer to GPU
        tokenized_multimer = {key: tensor.cuda() for key, tensor in tokenized_multimer.items()}

        #Predict the structure
        with torch.no_grad():
            output = model(**tokenized_multimer)

        #Remove the linker from structure
        linker_mask = torch.tensor(mask_lst)[None, :, None] ###CHANGE THIS

        output['atom37_atom_exists'] = output['atom37_atom_exists'] * linker_mask.to(
            output['atom37_atom_exists'].device)

    else:

        #The input is tokenized
        tokenized_input = tokenizer([protein_seq], return_tensors="pt", add_special_tokens=False)['input_ids']

        #Move tokenized input to GPU
        tokenized_input = tokenized_input.cuda()

        #Get the output for the given tokenized input
        with torch.no_grad():
            output = model(tokenized_input)

    #Convert output to pdb
    pdb = convert_outputs_to_pdb(output)

    #Write pdb to file
    with open(sequences_to_predict[0][i] + ".pdb", "w") as f:
        f.write("".join(pdb))