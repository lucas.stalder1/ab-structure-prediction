#!/usr/bin/env bash


WORK_DIR="$PWD"

if test -f "max_template_date.txt"; then
    echo "max_template_date file was found!"
    MAX_TEMP_DATE=$(cat max_template_date.txt)
fi


echo "Max template date is set to $MAX_TEMP_DATE"

while IFS="" read -r p || [ -n "$p" ]
do
  echo "$(pwd)/${p}.fasta"
  echo $WORK_DIR
  echo "$(pwd)/${p}.sbatch"
  echo $MAX_TEMP_DATE
  ./setup_alphafold_platypus.sh -f "$(pwd)/${p}.fasta" -w $WORK_DIR -b "SLURM" -s es_reddy  --max_template_date $MAX_TEMP_DATE
  sbatch  < "$(pwd)/${p}.sbatch"

done < barcodes.txt
